from django_filters.rest_framework import FilterSet
from .models import Term
import django_filters


class TermFilter(FilterSet):
    term_startswith = django_filters.CharFilter(name='term',
                                                lookup_expr='istartswith')

    class Meta:
        model = Term
        fields = ['term', 'description', 'term_startswith', ]
