var resultsPerPage = 25;  // number of results per page in a collection
var termsPagination;
var relationshipsPagination;

var currentTermLookup = {
    'limit': resultsPerPage,
    'offset': 0
};
var currentRelationshipLookup = {
    'limit': resultsPerPage,
    'offset': 0
};

function insertTerms(data) {
    var termsTable = $('#terms-table');
    var pagination = termsPagination;
    var termsHTML = '';
    $.each(data.results, function (k, v) {
        var term = v.term;
        if (v.approved === false) {
            term += ' (pending)'
        }
        termsHTML += '<li> <div class="collapsible-header"><i class="material-icons">assignment</i>' + term + '</div> <div class="collapsible-body"> <div class="row"> <p>' + v.description + '</p> </div> <div class="row"> <div class="col s2 offset-s1"> <div class="chip"> <a class="grey-text text-darken-1" href="' + v.license_url + '" target="_blank">' + v.license + '</a> </div> </div> <div class="col s3 offset-s3"> <a href="' + termsEditURL + '?id=' + v.id + '" class="waves-effect waves-light btn right"><i class="material-icons left">mode_edit</i>Edit</a> </div> <div class="col s3"> <button class="waves-effect waves-light btn" type="button"><i class="material-icons left">equalizer</i>Visualize</button> </div> </div> </div> </li>';
    });
    termsTable.html(termsHTML);
    var count = data.count;
    if (count === 0) {
        count = 1;
    }
    var currentPages = pagination.find('li').filter(function () {
        return $(this).find('i').length === 0;
    });
    var currentPageCount = currentPages.length;
    var newPageCount = Math.round(count / resultsPerPage) + 1;
    if (count % resultsPerPage === 0) {
        newPageCount--;
    }
    if (currentPageCount > newPageCount) {
        currentPages.each(function () {
            if (parseInt($(this).text()) > newPageCount) {
                $(this).remove();
            }
        });
    } else if (currentPageCount < newPageCount) {
        var numberOfPagesToAdd = newPageCount - currentPageCount;
        let lastCurrentPage = currentPages.last();
        var newPagesHTML = '';
        var currentNewPageNumber = parseInt(lastCurrentPage.text());
        while (numberOfPagesToAdd > 0) {
            currentNewPageNumber++;
            newPagesHTML += '<li class="waves-effect"><a href="#!">' + currentNewPageNumber.toString() + '</a></li>';
            numberOfPagesToAdd--;
        }
        lastCurrentPage.after(newPagesHTML);
    }
    // enable and disable left and right pagination arrows as necessary
    var currentSelectedPage = pagination.find('.active');
    if (parseInt(currentSelectedPage.text()) === newPageCount) {
        pagination.find('li').last().addClass('disabled');
    } else {
        pagination.find('li').last().removeClass('disabled');
    }
    if (parseInt(currentSelectedPage.text()) === 1) {
        pagination.find('li').first().addClass('disabled');
    } else {
        pagination.find('li').first().removeClass('disabled');
    }
    $('html, body').animate({
            scrollTop: $('#vocab-nav-tabs').offset().top + 'px'
        }, 'fast');
    $('.indeterminate-loader').parent().remove();
}

function loadTerms() {
    var pagination = termsPagination;
    var startsWith = $('.term-filter').find('.active').text();
    var currentPage;
    var oldStartsWith = currentTermLookup['term_startswith'];
    if (typeof oldStartsWith) {
        oldStartsWith = 'All';
    }
    if (startsWith !== oldStartsWith) {
        pagination.find('li').filter(function () {
            return $(this).find('a').text() === '1';
        }).addClass('active');
        currentPage = 1;
    } else {
        currentPage = termsPagination.find('.active').text();
        currentPage = parseInt(currentPage);
    }
    // just get the results for the current page
    currentTermLookup['offset'] = (currentPage - 1) * resultsPerPage;
    if (startsWith === 'All') {
        delete currentTermLookup['term_startswith'];
    } else {
        currentTermLookup['term_startswith'] = startsWith;
    }
    let loaderContainer = '<div class="row"><div class="col m12 indeterminate-loader"></div></div>';
    $('#terms-table')
        .after(loaderContainer)
        .before(loaderContainer);
    insertIndeterminateLoader($('.indeterminate-loader'));
    $.ajax({
        url: termsListCreateURL,
        type: 'GET',
        data: currentTermLookup
    })
        .done(insertTerms);
}

function insertRelationships (data) {
    var relationshipsTable = $('#relationships-table');
    var pagination = relationshipsPagination;
    var relationshipsHTML = '';
    $.each(data.results, function(k, v) {
        var relationship = v.relationship;
        if (v.approved === false) {
            relationship += ' (pending)'
        }
        relationshipsHTML += '<li><div class="collapsible-header"><i class="material-icons">library_books</i>' + v.relationship + '</div><div class="collapsible-body"><p>' + v.description + '</p></div></li>';
        });
    relationshipsTable.html(relationshipsHTML);
    var count = data.count;
    if (count === 0) {
        count = 1;
    }
    var currentPages = pagination.find('li').filter(function () {
        return $(this).find('i').length === 0;
    });
    var currentPageCount = currentPages.length;
    var newPageCount = Math.round(count / resultsPerPage) + 1;
    if (count % resultsPerPage === 0) {
        newPageCount--;
    }
    if (currentPageCount > newPageCount) {
        currentPages.each(function () {
            if (parseInt($(this).text()) > newPageCount) {
                $(this).remove();
            }
        });
    } else if (currentPageCount < newPageCount) {
        var numberOfPagesToAdd = newPageCount - currentPageCount;
        let lastCurrentPage = currentPages.last();
        var newPagesHTML = '';
        var currentNewPageNumber = parseInt(lastCurrentPage.text());
        while (numberOfPagesToAdd > 0) {
            currentNewPageNumber++;
            newPagesHTML += '<li class="waves-effect"><a href="#!">' + currentNewPageNumber.toString() + '</a></li>';
            numberOfPagesToAdd--;
        }
        lastCurrentPage.after(newPagesHTML);
    }
    // enable and disable left and right pagination arrows as necessary
    var currentSelectedPage = pagination.find('.active');
    if (parseInt(currentSelectedPage.text()) === newPageCount) {
        pagination.find('li').last().addClass('disabled');
    } else {
        pagination.find('li').last().removeClass('disabled');
    }
    if (parseInt(currentSelectedPage.text()) === 1) {
        pagination.find('li').first().addClass('disabled');
    } else {
        pagination.find('li').first().removeClass('disabled');
    }
    $('html, body').animate({
            scrollTop: $('#vocab-nav-tabs').offset().top + 'px'
        }, 'fast');
    $('.indeterminate-loader').parent().remove();
}

function loadRelationships() {
    var pagination = relationshipsPagination;
    var startsWith = $('.relationship-filter').find('.active').text();
    var currentPage = relationshipsPagination.find('.active').text();
    currentPage = parseInt(currentPage);
    // just get the results for the current page
    currentRelationshipLookup['offset'] = (currentPage - 1) * resultsPerPage;
    let loaderContainer = '<div class="row"><div class="col m12 indeterminate-loader"></div></div>';
    $('#relationships-table')
        .after(loaderContainer)
        .before(loaderContainer);
    insertIndeterminateLoader($('.indeterminate-loader'));
    $.ajax({
        url: relationshipsListCreateURL,
        type: 'GET',
        data: currentRelationshipLookup
    })
        .done(insertRelationships);
}

$(document).ready(function () {
    termsPagination = $('.terms-pagination');
    relationshipsPagination = $('.relationships-pagination');
    $('#nav-vocab-main').parent().addClass('active');
    $('select').material_select();
    $('.modal-trigger').leanModal();
    $('.term-filter')
        .find('li').on('click', function (e) {
            e.preventDefault();
            $('.term-filter').find('.active').removeClass('active');
            $(this).addClass('active');
            loadTerms();
        });
    termsPagination
        .on('click', 'li', function (e) {
            e.preventDefault();
            termsPagination.find('.active').removeClass('active');
            $(this).addClass('active');
            loadTerms();
        });
    $('#term-input').on('change', function (e) {
        if (!$.inArray($(this).val().trim(), terms)) {
            document.getElementById('term-input').setCustomValidity('Not valid');
        } else {
            document.getElementById('term-input').setCustomValidity('');
        }
    });
    relationshipsPagination
        .on('click', 'li', function (e) {
            e.preventDefault();
            relationshipsPagination.find('.active').removeClass('active');
            $(this).addClass('active');
            loadRelationships();
        });
    $('#relationship-input').on('change', function (e) {
        if (!$.inArray($(this).val().trim(), relationships)) {
            document.getElementById('relationship-input').setCustomValidity('Not valid');
        } else {
            document.getElementById('relationship-input').setCustomValidity('');

        }
    });
    $('#term-license-input').on('change', function (e) {
        if ($(this).val() === 'Copyright') {
            $(this).parent().parent().parent().parent().after('<div class="row" id="term-copyrighting-org-row"> <div class="col s12"> <div class="input-field"> <input type="text" class="materialize-textarea validate" id="term-copyrighting-org-input" maxlength="255" placeholder="Copyrighting Organization" required> <label for="term-copyrighting-org-input">Copyrighting Organization</label> </div> </div> </div>');
            Materialize.updateTextFields();
        } else {
            $('#term-copyrighting-org-row').remove();
        }
    });
    $('#add-term-form').on('submit', function (e) {
        e.preventDefault();
        var data = {
            term: $('#term-input').val().trim(),
            description: $('#term-description-input').val(),
            domain: $('#term-domain-input').val()
        };
        var license = $('#term-license-input').val();
        if (license !== 'Choose your license') {
            data.license = license;
        }
        $.ajax({
            url: termsListCreateURL,
            type: 'POST',
            data: data
        })
            .done(function (data) {
                $('#add-term-modal').closeModal();
                loadTerms();
        });
    });
    $('#submit-add-relationship-btn').on('click', function (e) {
        e.preventDefault();
        var data = {
            relationship: $('#relationship-input').val().trim(),
            description: $('#relationship-description-input').val()
        };
        $.ajax({
            url: relationshipsListCreateURL,
            type: 'POST',
            data: data
        })
            .done(function (data) {
                $('#add-relationship-modal').closeModal();
                loadRelationships();
            });
    });
    $('#submit-add-domain-btn').on('click', function (e) {
        e.preventDefault();
        let manager = $('#domain-manager-input').find('option:selected').data('id');
        var data = {
            'name': $('#domain-name-input').val(),
            'description': $('#domain-description-input').val(),
            'manager': manager
        };
        $.ajax({
            url: domainsListCreateURL,
            type: 'POST',
            data: data
        })
            .fail(function () {

            })
            .done(function (data) {
                $('#add-domain-modal').closeModal();
                $.ajax({
                    url: domainsListCreateURL,
                    type: 'GET'
                })
                    .done(function (data) {
                        domains = [];
                        var domainsTable = $('#domains-table');
                        var domainsHTML = '';
                        $.each(data, function (k, v) {
                            domains.push(v.name);
                            domainsHTML += '<li><div class="collapsible-header"><i class="material-icons">group_work</i>' + v.name + '</div><div class="collapsible-body"><p>' + v.description + '</p></div></li>';
                        });
                        domainsTable.html(domainsHTML);
                    });
            });
    });
});
