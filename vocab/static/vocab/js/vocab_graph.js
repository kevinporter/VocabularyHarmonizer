$(document).ready(function () {
  $('#nav-vocab-graph').parent().addClass('active');
  $('.button-collapse').sideNav();
  let bottom = $('footer').position().top + 20;
  // there's some stupid extra 20 pixels I can't account for
  var height = bottom - $('#top-navbar').height();

  $('#side-navbar')
  .css({
    'top': function () {
      return $('#top-navbar').height();
    },
    'height': height,
    'width': '30%',
    'max-width': '400px',
    'min-width': '250px',
    'overflow-x': 'hidden'
  });
  var width = $(window).width() - $('#side-navbar').width();
  var left = $('#side-navbar').width();
  var top = $('#top-navbar').height();

  var color = d3.scale.category20();

  var force = d3.layout.force()
  .charge(-120)
  .linkDistance(30)
  .size([width, height]);

  var svg = d3.select(".chart-container")
    .append('svg')
    .attr('preserveAspectRatio', 'xMinYMin meet')
    .attr('viewBox', '0 0 ' + (window.screen.availWidth / 2) + ' ' + (window.screen.availHeight / 2))
    .classed('svg-content', true);

  d3.json(jsonFile, function(error, graph) {

    force
    .nodes(graph.nodes)
    .links(graph.links)
    .start();

    var link = svg.selectAll(".link")
    .data(graph.links)
    .enter().append("line")
    .attr("class", "link")
    .style("stroke-width", function(d) { return Math.sqrt(d.value); });

    var node = svg.selectAll(".node")
    .data(graph.nodes)
    .enter().append("circle")
    .attr("class", "node")
    .attr("r", 5)
    .style("fill", function(d) { return color(d.group); })
    .call(force.drag);

    node.append("title")
    .text(function(d) { return d.name; });

    force.on("tick", function() {
      link.attr("x1", function(d) { return d.source.x; })
      .attr("y1", function(d) { return d.source.y; })
      .attr("x2", function(d) { return d.target.x; })
      .attr("y2", function(d) { return d.target.y; });

      node.attr("cx", function(d) { return d.x; })
      .attr("cy", function(d) { return d.y; });
    });
  });
  $('.chart-container').css({
    'left': function() {
      return $('#side-navbar').width();
    },
    'top': function () {
      return $('#top-navbar').height();
    },
    'width': width,
    'height': height
  });
  $('.svg-content').css({
    'height': height,
    'width': width
  });
  $(window).resize(function () {
    let bottom = $('.page-footer').position().top;
    var height = bottom - $('#top-navbar').height() + 20;
    $('#side-navbar')
    .css({
      'top': function () {
        return $('#top-navbar').height();
      },
      'height': height,
      'width': '30%',
      'max-width': '400px',
      'min-width': '250px',
      'overflow-x': 'hidden'
    });
    var width = $(window).width() - $('#side-navbar').width();
    $('.chart-container')
      .css({
        'left': function() {
          return $('#side-navbar').width();
        },
        'top': function () {
          return $('#top-navbar').height();
        },
        'width': width,
        'height': height
      });
      $('.svg-content').css({
        'height': height,
        'width': width
      });
  });

});
