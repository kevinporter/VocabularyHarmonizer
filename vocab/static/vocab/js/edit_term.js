function addTermPill (term) {
    var that = $('#object-terms-input');
    that.val('');
    var tagHTML = '<div class="chip term-tag">' + term + '<i class="material-icons">close</i></div>';
    if ($('.term-tag').length === 0) {
        that.parent().prepend(tagHTML);
    } else {
        $('.term-tag').last().after(tagHTML);
    }
    $('#object-terms-input').css({
        'text-indent': function () {
            let lastTag = $('.term-tag').last();
            let padding = parseInt(lastTag.css('padding-left').replace(/px/, '')) * 2;
            return lastTag.position().left + lastTag.width() + padding;
        }
    });
    $('.term-tag').css('top', function () {
        return that.css('top');
    });
    $('#faux-typeahead')
        .css({
            'top': function () {
                let input = $('#object-terms-input');
                let margin = parseInt(input.css('margin-bottom').replace(/px/, ''));
                // I haven't figured out why I need to multiply by 4
                return input.offset().top + input.height() - (margin * 4);
            }
        });
}

$(document).ready(function() {
    $('select').material_select();
    $('#nav-vocab-main').parent().addClass('active');
    $('#add-relationship-btn').on('click', function () {
        $('#add-relationship-modal').openModal();
    });
    $('#object-terms-input')
        .on('keyup', function(e) {
            var value = $('#object-terms-input').val();
            if (value !== '') {
                $.ajax({
                    url: termListCreateURL,
                    type: 'GET',
                    data: {search: value}
                })
                    .done(function (data) {
                        var typeahead = $('#faux-typeahead');
                        var typeaheadHTML = '';
                        $.each(data, function (k, v) {
                            typeaheadHTML +='<li class="typeahead-result"><a href="#">' + v.term + '</a></li>'
                        });
                        typeahead.html(typeaheadHTML);
                        $('#add-relationship-modal')
                            .css('height', function () {
                                return $(this).height() + typeahead.height();
                            });
                        $('.typeahead-result')
                            .click(function (e) {
                                e.preventDefault();
                                addTermPill($(this).text());
                                typeahead.css('display', 'none');
                            });
                    });
                $('#faux-typeahead')
                    .css({
                        'display': 'block',
                        'opacity': 1,
                        'z-index': '999'
                    })
                    .css({
                        'top': function () {
                            let input = $('#object-terms-input');
                            let margin = parseInt(input.css('margin-bottom').replace(/px/, ''));
                            // I haven't figured out why I need to multiply by 4
                            return input.offset().top + input.height() - (margin * 4);
                        }
                    });
            }
            if (e.which === 188) {
                let term = $(this).val().replace(/,/g, '');
                addTermPill(term);
            }
        });
    $('.object-terms-container')
        .on('click', '.term-tag i', function(e) {
            $('#object-terms-input').css({
                'text-indent': function () {
                    let lastTag = $('.term-tag').last();
                    return lastTag.position().left;
                }
            });
        });
    $('#save-relationship-btn').click(function (e) {
        var subject = $('#add-relationship-modal').find('.term-title').text();
        var predicate = $('.relationship-selection').find('input').val();
        var triples = [];
        $('.term-tag').each(function () {
            let text = $(this).clone()    //clone the element
                .children() //select all the children
                .remove()   //remove all the children
                .end()  //again go back to selected element
                .text();
            triples.push({'subject': subject, 'predicate': predicate, 'object': text});
            $('#term-relationships').append('<p>' + subject + '&emsp;' + predicate + '&emsp;' + text);
        });
        if (triples.length === 1) {
            triples = triples[0];
        }
        $.ajax({
            url: tripleListCreateURL,
            type: 'POST',
            data: JSON.stringify(triples),
            contentType: 'application/json'
        })
            .done(function (data) {
                $('#add-relationship-modal').closeModal();
            });
    });
});
