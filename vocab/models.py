"""
Models for the vocab app.
@author Kevin Porter
"""
from __future__ import unicode_literals

from django.db import models
from django.apps import apps
from django.contrib.auth.models import Group, User
from django.contrib.contenttypes.models import ContentType
from common.models import License
from fuzzywuzzy import fuzz


class Domain(models.Model):
    """
    A domain in the vocabulary.
    @author Kevin Porter
    """
    name = models.CharField(max_length=255, unique=True)
    group = models.ForeignKey(Group, default=1)
    description = models.CharField(max_length=512)
    manager = models.ForeignKey(User)

    def __str__(self):
        return self.name

    def create(self, *args, **kwargs):
        """
        Enhances the default create method to automatically create a group for
        the domain.
        @author Kevin Porter
        """
        try:
            g = Group.objects.get(name=self.name)
        except Group.DoesNotExist:
            g = Group.objects.create(name=self.name)
        return super(Domain, self).create(group=g, *args, **kwargs)


class Term(models.Model):
    """
    An atomic vocabulary term. (e.g., tension_test, modulus_of_elasticity, etc.)
    @author Kevin Porter
    """
    term = models.CharField(max_length=255, unique=True)
    description = models.CharField(max_length=2048)
    license = models.ForeignKey(License, blank=True, null=True)
    domain = models.ForeignKey(Domain)
    approved = models.BooleanField(default=False)
    date_created = models.DateTimeField(auto_now_add=True)
    date_modified = models.DateTimeField(auto_now=True)
    date_status_change = models.DateTimeField(blank=True, null=True)  # change from/to approved/rejected

    def __str__(self):
        val = self.term
        if not self.approved:
            val += ' (pending)'
        return val

    def license_url(self):
        if self.license:
            url = self.license.url
        else:
            url = '#'
        return url

    def get_primary_relationships(self):
        """
        Provides all triples for which this term is a subject.
        :return: All triples for which this term is a subject.

        @author Kevin Porter
        """
        TermRelationshipTerm = apps.get_model(app_label='vocab',
                                              model_name='termrelationshipterm')
        qs = TermRelationshipTerm.objects.filter(subject=self)
        return qs

    def get_secondary_relationships(self):
        """
        Provides all triples for which this term is an object.
        :return: All triples for which this term is an object.

        @author Kevin Porter
        """
        TermRelationshipTerm = apps.get_model(app_label='vocab',
                                              model_name='termrelationshipterm')
        qs = TermRelationshipTerm.objects.filter(object=self)
        return qs

    def relationships(self, relationships=None):
        """
        A serializer function for retrieving or saving the relationships for a
        term.
        :return: A list of all the primary relationships for this term.
        """
        if relationships:
            TermRelationshipTerm = apps.get_model(
                app_label='vocab', model_name='termrelationshipterm')
            Relationship = apps.get_model(app_label='vocab',
                                          model_name='relationship')
            for relationship in relationships:
                try:
                    r = Relationship.objects.get(
                        relationship=relationship['relationship'])
                except Relationship.DoesNotExist:
                    raise Exception('Relationships must be manually added to '
                                    'the dictionary.')
                try:
                    o = Term.objects.get(term=relationship['object'])
                except Term.DoesNotExist:
                    raise Exception('Object vocab must be manually added to '
                                    'the dictionary.')
                TermRelationshipTerm.objects.get_or_create(subject=self,
                                                           predicate=r,
                                                           object=o)
        from .serializers import TripleSerializer
        rels = self.get_primary_relationships()
        return TripleSerializer(rels, many=True).data

    def get_similarity_suggestions(self):
        terms = Term.objects.all().exclude(id=self.id)
        term_outputs = []
        for term in terms:
            data = {
                'term': term,
                'similarity': fuzz.ratio(self.description + self.term, term.description + term.term),
            }
            term_outputs.append(data)
        return sorted(term_outputs, key=lambda k: k['similarity'], reverse=True)


class Relationship(models.Model):
    """
    A relationship between vocab. (e.g., synonymous_to, similar_to, related_to,
    etc.)
    @author Kevin Porter
    """
    relationship = models.CharField(max_length=255, unique=True)
    description = models.CharField(max_length=2048)
    license = models.ForeignKey(License, blank=True, null=True)
    approved = models.BooleanField(default=False)
    date_created = models.DateTimeField(auto_now_add=True)
    date_modified = models.DateTimeField(auto_now=True)
    date_status_change = models.DateTimeField(blank=True, null=True)  # change from/to approved/rejected

    def __str__(self):
        val = self.relationship
        if not self.approved:
            val += ' (pending)'
        return val

    def get_triples(self):
        TermRelationshipTerm = apps.get_model(app_label='vocab',
                                              model_name='termrelationshipterm')
        qs = TermRelationshipTerm.objects.filter(predicate=self)
        return qs


class TermRelationshipTerm(models.Model):
    """
    A pseudo-triple statement. This structure is bound to change.
    @author Kevin Porter
    """
    subject = models.ForeignKey(Term, related_name='subject')
    predicate = models.ForeignKey(Relationship)
    object = models.ForeignKey(Term, related_name='object')
    date_created = models.DateTimeField(auto_now_add=True)
    date_modified = models.DateTimeField(auto_now=True)
    date_status_change = models.DateTimeField(blank=True, null=True)  # change from/to approved/rejected

    def __str__(self):
        return str(self.subject) + ' ' + str(self.predicate) + ' ' + \
                str(self.object)


class EditRecord(models.Model):
    content_type = models.ForeignKey(ContentType)
    object_id = models.PositiveIntegerField()
    date = models.DateTimeField(auto_now_add=True)
    user = models.ForeignKey(User)
    description = models.CharField(max_length=2048)

    def get_object(self):
        obj = self.content_type.get_object_for_this_type(id=self.object_id)
        return obj


class RejectRecord(models.Model):
    content_type = models.ForeignKey(EditRecord)
    comments = models.TextField()
    date = models.DateTimeField(auto_now_add=True)
    manager = models.ForeignKey(User)


class ApproveRecord(models.Model):
    content_type = models.ForeignKey(EditRecord)
    date = models.DateTimeField(auto_now_add=True)
    manager = models.ForeignKey(User)
