from django.contrib import admin
from .models import Term, Relationship, Domain


admin.site.register(Term)
admin.site.register(Relationship)
admin.site.register(Domain)
