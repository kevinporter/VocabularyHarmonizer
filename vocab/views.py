import math
from collections import OrderedDict
from django.contrib.auth.models import User
from rest_framework import generics, filters
from rest_framework.renderers import TemplateHTMLRenderer
from rest_framework.response import Response
from rest_framework import status
from vocabulary_harmonizer import helpers
from common.models import License
from .filters import TermFilter
from .models import Term, Relationship, TermRelationshipTerm, Domain
from .serializers import (TermSerializer, RelationshipSerializer,
                          DomainSerializer, TripleSerializer, )


class TermListCreateView(generics.ListCreateAPIView):
    serializer_class = TermSerializer
    queryset = Term.objects.all().order_by('term')
    filter_backends = (filters.SearchFilter, filters.DjangoFilterBackend,
                      filters.OrderingFilter, )
    filter_class = TermFilter
    search_fields = ('term', 'description', 'license__name', )


class TermDetailView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = TermSerializer
    queryset = Term.objects.all()


class RelationshipListCreateView(generics.ListCreateAPIView):
    serializer_class = RelationshipSerializer
    queryset = Relationship.objects.all()


class RelationshipDetailView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = RelationshipSerializer
    queryset = Relationship.objects.all()


class TripleListCreateView(generics.ListCreateAPIView):
    serializer_class = TripleSerializer
    queryset = Relationship.objects.all()


class TripleDetailView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = TripleSerializer
    queryset = TermRelationshipTerm.objects.all()


class DomainListCreateView(generics.ListCreateAPIView):
    serializer_class = DomainSerializer
    queryset = Domain.objects.all()


class DomainDetailView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = DomainSerializer
    queryset = Domain.objects.all()


class MainPageView(generics.RetrieveAPIView):
    renderer_classes = (TemplateHTMLRenderer, )

    def get(self, request, *args, **kwargs):
        default_page_size = 25
        # at least one needs to be float to override implicit rounding
        number_of_term_pages = int(math.ceil(len(
            Term.objects.all()) / float(default_page_size)))
        term_pages = [num for num in range(1, number_of_term_pages + 1)]
        number_of_relationship_pages = int(math.ceil(len(
            Relationship.objects.all()) / float(default_page_size)))
        relationship_pages = [num for num in range(1, number_of_relationship_pages + 1)]
        params = {
            'terms': Term.objects.all().order_by('term')[:default_page_size],
            'relationships': Relationship.objects.all()[:default_page_size],
            'domains': Domain.objects.all()[:25],
            'licenses': License.objects.all(),
            'users': User.objects.all(),
            'is_manager': helpers.is_manager(request.user),
            'pending': helpers.get_pending_count(request.user),
            'term_pages': term_pages,
            'relationship_pages': relationship_pages,
        }
        return Response(params, template_name='vocab/main_page.html',
                        status=status.HTTP_200_OK)


class VocabGraphView(generics.RetrieveAPIView):
    renderer_classes = (TemplateHTMLRenderer, )

    def get(self, request, *args, **kwargs):
        params = {
            'terms': Term.objects.all(),
            'relationships': Relationship.objects.all(),
            'domains': Domain.objects.all(),
            'is_manager': helpers.is_manager(request.user),
            'pending': helpers.get_pending_count(request.user),
        }
        return Response(params, template_name='vocab/vocab_graph.html',
                        status=status.HTTP_200_OK)


class GraphView(generics.RetrieveAPIView):
    renderer_classes = (TemplateHTMLRenderer, )

    def get(self, request, *args, **kwargs):
        params = {
            'terms': Term.objects.all(),
            'relationships': Relationship.objects.all(),
            'domains': Domain.objects.all(),
        }
        return Response(params, template_name='vocab/graph.html',
                        status=status.HTTP_200_OK)


class EditTermView(generics.RetrieveAPIView):
    renderer_classes = (TemplateHTMLRenderer, )

    def get(self, request, *args, **kwargs):
        term_id = request.query_params.get('id')
        term = Term.objects.get(id=term_id)
        params = {
            'terms': Term.objects.all(),
            'relationships': Relationship.objects.all(),
            'licenses': License.objects.all(),
            'term': term,
            'term_rels': term.get_primary_relationships(),
            'domains': Domain.objects.all(),
            'is_manager': helpers.is_manager(request.user),
            'pending': helpers.get_pending_count(request.user),
        }
        return Response(params, template_name='vocab/edit_term.html',
                        status=status.HTTP_200_OK)


class PendingView(generics.RetrieveAPIView):
    renderer_classes = (TemplateHTMLRenderer, )

    def get(self, request, *args, **kwargs):
        default_page_size = 25
        term_qs = Term.objects.filter(approved=False)
        relationship_qs = Relationship.objects.filter(approved=False)
        pending_items_list = list(term_qs) + list(relationship_qs)
        pending_items_transform = []
        for item in pending_items_list:
            pending_items_transform.append({
                'name': item.__str__(),
                'type': type(item).__name__,
                'description': item.description,
            })
        # at least one needs to be float to override implicit rounding
        pending_pages = int(math.ceil(len(pending_items_list) / float(default_page_size)))
        pending_pages = [num for num in range(1, pending_pages + 1)]
        params = {
            'domains': Domain.objects.all()[:25],
            'licenses': License.objects.all(),
            'users': User.objects.all(),
            'is_manager': helpers.is_manager(request.user),
            'pending': helpers.get_pending_count(request.user),
            'pending_pages': pending_pages,
            'pending_items': pending_items_transform,
        }
        return Response(params, template_name='vocab/pending.html',
                        status=status.HTTP_200_OK)
