from django.core.management.base import BaseCommand, CommandError
from django.contrib.auth.models import User
from vocab.models import Term, Relationship, Domain

class Command(BaseCommand):
    help = 'Closes the specified poll for voting'

    def handle(self, *args, **options):
        user = User.objects.last()  # assume most recently created user is you
        if (user.is_anonymous()):
            user = User.objects.create_user(username='test',
                                            password='password')
        for i in range(0, 100):
            d = Domain.objects.create(name=i, description=i, manager=user)
            Relationship.objects.create(relationship=i, description=i)
            approved = i % 2 == 0
            Term.objects.create(term=i, description=i, domain=d,
                                approved=approved)
