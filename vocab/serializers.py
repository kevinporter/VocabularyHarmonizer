from rest_framework import serializers
from django.contrib.auth.models import User
from common.models import License
from .models import Term, Relationship, TermRelationshipTerm, Domain


class TermSerializer(serializers.ModelSerializer):
    license = serializers.SlugRelatedField(slug_field='name',
                                           queryset=License.objects.all(),
                                           required=False)
    domain = serializers.SlugRelatedField(slug_field='name',
                                          queryset=Domain.objects.all())

    def create(self, validated_data):
        term = super(TermSerializer, self).create(validated_data)
        if 'relationships' in validated_data:
            term.relationships(validated_data.get('relationships'))
        return term

    def update(self, instance, validated_data):
        term = super(TermSerializer, self).update(instance, validated_data)
        if 'relationships' in validated_data:
            term.relationships(validated_data.get('relationships'))
        return term

    class Meta:
        model = Term
        fields = ('id', 'term', 'relationships', 'description', 'license',
                  'license_url', 'domain', 'approved', )


class RelationshipSerializer(serializers.ModelSerializer):

    class Meta:
        model = Relationship
        fields = ('relationship', 'description', 'approved', )


class DomainSerializer(serializers.ModelSerializer):
    manager = serializers.SlugRelatedField(slug_field='username',
                                           queryset=User.objects.all())

    class Meta:
        model = Domain
        fields = ('name', 'description', 'manager', )


class TripleSerializer(serializers.ModelSerializer):
    predicate = serializers.SlugRelatedField(
        slug_field='relationship', queryset=Relationship.objects.all())
    object = serializers.SlugRelatedField(slug_field='term',
                                          queryset=Term.objects.all())
    subject = serializers.SlugRelatedField(slug_field='term',
                                           queryset=Term.objects.all())

    class Meta:
        model = TermRelationshipTerm
        fields = ('predicate', 'object', 'subject', )
