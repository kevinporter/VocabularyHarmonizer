$(document).ready(function() {
    $('body')
        .removeClass('white')
        .addClass('blue darken-4');
    $('div.container').removeClass('container');
    $('.card-panel').on('click', function(e) {
        e.preventDefault();
        window.location = $(this).data('link');
    });
});
