"""
Helper functions and classes to be used throughout the project.
@author Kevin Porter
"""
from vocab.models import Domain, Relationship, Term


def is_manager(user):
    """
    Determines whether or not a user is a manager of any domains. Useful for
    determining whether or not to display the pending page.
    @author Kevin Porter
    """
    if user.is_anonymous():
        manager = False
    else:
        manager = len(Domain.objects.filter(manager=user)) > 0
    return manager


def get_pending_count(user):
    """
    Get the number of objects pending approval by the given user.
    @author Kevin Porter
    """
    if user.is_anonymous():
        count = 0
    else:
        domains = Domain.objects.filter(manager=user)
        terms = len(Term.objects.filter(domain__in=domains, approved=False))
        relationships = len(Relationship.objects.filter(approved=False))
        count = terms + relationships
    return count
